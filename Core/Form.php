<?php 

namespace App\Core;

class Form 
{
    private $formCode = '';

    /**
     * Génère le formulaire html
     *
     * @return string
     */
    public function create ()
    {
        return $this->formCode;
    }

    /**
     * valide si tous les champs sont remplis
     *
     * @param array $form tableau issu du formulaire ($Post)
     * @param array $champ tableau listant les champs obligatoires
     * @return bool
     */
    public static function validate(array $form, array $champs)
    {
        //on parcourt les champs
        foreach ($champs as $champ){
            //si le champ est absent ou vide 
            if(!isset($form[$champ]) || empty($form[$champ])){
                //on sort en retournant false 
                return false;
            }    

            if($form[$champ] == $form['email']){
                $form[$champ] = filter_var($form[$champ], FILTER_SANITIZE_EMAIL);
            }   
            
        }    
        return true;
    }

    /**
     * valide si tous les champs sont remplis
     *
     * @param array $form tableau issu du formulaire ($Post)
     * @param array $champ tableau listant les champs obligatoires
     * @return bool
     */
    public static function validateRegister(array $form, array $champs)
    {
        //on parcourt les champs
        foreach ($champs as $champ){
            //si le champ est absent ou vide 
            if(!isset($form[$champ]) || empty($form[$champ])){
                //on sort en retournant false 
                return false;
            }    

            if($form[$champ] == $form['email']){
                $form[$champ] = filter_var($form[$champ], FILTER_SANITIZE_EMAIL);
            }   
            
        }
    
        if($form['pass2'] !== $form['password'] && strlen($form['password']) <= 5 || strlen($form['pass2']) <= 5){
            return false;
        }
        return true;
    }

    /**
     * Ajoute des attributs à la balise
     *
     * @param array $attributs tableau associatif ['class' => 'form-control']
     * @return string chaine de caractères
     */
    private function ajoutAttributs(array $attributs): string
    {
        //On va initialiser une chaîne de caractères 
        $str = '';

        // on liste des attributs "courts"  

        $courts = ['checked', 'disabled', 'readonly', 'required', 'autofocus', 'novalidate', 'formnovalidate', 'placeholder'];

        // on boucle sur le tableau d'attributs

        foreach ($attributs as $attribut => $valeur){
            //si l'attribut est dans la liste des attributs courts

            if(in_array($attribut, $courts) && $valeur == true){
                $str .= " $attribut";
            }
            else{
                // on ajoute attribut='valeur' 
                $str .= " $attribut=\"$valeur\"";
            }
        }

        return $str;
    }

    /**
     * fonction qui permet de créer la première balise form (balise ouverture)
     *
     * @param string $methode
     * @param string $action
     * @param array $attributs
     * @return Form
     */
    public function debutForm(string $methode = 'post', string $action = '#', array $attributs = []): self 
    {
        // on crée la balise form
        $this->formCode = "<form action='$action' method='$methode'";

        //on ajoute les attributs éventuels
        
        $this->formCode .= $attributs ? $this->ajoutAttributs($attributs).'>' : '>';        

        return $this;
    }

    /**
     * retourne la dernière balise form (balise fermante)
     *
     * @return Form
     */
    public function finForm()
    {
        $this->formCode .= '</form>';
        return $this;
    }

    /**
     * fonction pour ajouter des labels
     *
     * @param string $for
     * @param string $texte
     * @param array $attributs
     * @return Form
     */
    public function ajoutLabelFor(string $for, string $texte, array $attributs = []):self
    {
        //on ouvre la balise 
        $this->formCode .= "<label for='$for'";

        //on ajoute les attributs 

        $this->formCode .= $attributs ? $this->ajoutAttributs($attributs) : '';

        //on ajoute le texte 

        $this->formCode .= ">$texte</label>";


        return $this;
    }

    /**
     * methode qui ajoute un input en poo
     *
     * @param string $type
     * @param string $nom
     * @param array $attributs par défaut il est vide
     * @return Form
     */
    public function ajoutInput (string $type, string $nom, array $attributs = []):self
    {
        //On ouvre la balise 
        $this->formCode .= "<input type='$type' name='$nom'";

        //on ajoute les attributs et on ferme la balise input
        $this->formCode .= $attributs ? $this->ajoutAttributs($attributs).'>' : '>';

        return $this;
    }

    /**
     * methode qui retourne la balise textarea
     *
     * @param string $nom
     * @param string $valeur
     * @param array $attributs
     * @return Form
     */
    public function ajoutTextarea (string $nom, string $valeur='', array $attributs = [])
    {
        //on ouvre la balise textarea
        $this->formCode .= "<textarea name='$nom'";

        //on ajoute les attributs 

        $this->formCode .= $attributs ? $this->ajoutAttributs($attributs) : '';

        //on ajoute le texte 

        $this->formCode .= ">$valeur</textarea>";


        return $this;
    }

    /**
     * ajout d'une balise select avec les options
     *
     * @param string $nom
     * @param array $options
     * @param array $attributs
     * @return Form
     */
    public function ajoutSelect (string $nom, array $options, array $attributs = []):self
    {
        //on crée le select
        $this->formCode .= "<select name='$nom'";

        //on ajoute les attributs 

        $this->formCode .= $attributs ? $this->ajoutAttributs($attributs).'>' : '>';

        //on ajoute les options avec une boucle
        foreach ($options as $valeur => $texte ){
            $this->formCode .= "<option value=\"$valeur\">$texte</option>";
        }

        //on ferme le select

        $this->formCode .= '</select>';

        return $this;
    }

    /**
     * méthode d'ajout de bouton de formulaire
     * @param string $texte
     * @param array $attributs
     * @return Form
     */
    public function ajoutBouton (string $texte, array $attributs = []):self
    {
        //on ouvre la balise button
        $this->formCode .= "<button ";

        //on ajoute les attributs 

        $this->formCode .= $attributs ? $this->ajoutAttributs($attributs) : '';

        //on ferme la balise avant on ajoute le texte
        $this->formCode .= ">$texte</button>";
        
        return $this;
    }

}