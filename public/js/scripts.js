//on attend que le DOM soit chargé

window.onload = () => {
    let boutons = document.querySelectorAll(".form-check-input");

    //on écoute sur les boutons actifs l'événement click

    for(let bouton of boutons) {
        bouton.addEventListener("click", activer);
    }
}

function activer () {
    //on attrape la requête

    let xmlhttp = new XMLHttpRequest();

    //on envoit une requête dans l'url pour enclencher activeAnnonce dans l'admin controller

    xmlhttp.open('GET', '/admin/activeAnnonce/'+ this.dataset.id);

    xmlhttp.send();
}