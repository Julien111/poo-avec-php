<?php
namespace App\Controllers;

use App\Core\Form;
use App\Models\UsersModel;

class UsersController extends Controller
{
    /**
     * Connexion des utilisateurs
     *
     * @return void
     */
    public function login ()
    {
        //on vérifie si le formulaire est complet
        //var_dump($_SESSION['user']['id']);

        if(Form::validate($_POST, ['email', 'password'])){        
            $userModel = new UsersModel;
            $userArray = $userModel->findOneByEmail(strip_tags($_POST['email']));

            // Si l'utilisateur n'existe pas
            //On envoie un message de session 
            if(!$userArray){
                $_SESSION['erreur'] = 'L\'adresse e-mail et/ou le mot de passe est incorrect';
                header('Location: /users/login');
                exit;
            }

            //le formulaire est complet
            //On va chercher dans la base de données l'email et l'id
			
			//l'utilisateur existe 
            $user = $userModel->hydrate($userArray);

            //on doit vérifier que le mot de passe est correct 

            if(password_verify($_POST['password'], $user->getPassword())){
                //mot de passe ok
                //On crée la session
                $user->setSession();
                header('Location: /');
                exit;
            }
            else{
                $_SESSION['erreur'] = 'L\'adresse e-mail et/ou le mot de passe est incorrect';
                header('Location: /users/login');
                exit;
            }


        }
        
        $form = new Form;
        $form->debutForm()->ajoutLabelFor('email', 'E-mail :')
        ->ajoutInput('email', 'email', ['class' => 'form-control', 'placeholder' => 'Email', "id" => 'email'])
        ->ajoutLabelFor('password', 'Mot de passe :')
        ->ajoutInput('password', 'password', ['id' => 'pass', "class" => "form-control", 'placeholder' => 'Mot de passe'])
        ->ajoutBouton('Me connecter', ['class' => 'btn btn-primary'])
        ->finForm();

        $this->render('users/login', ['loginForm' => $form->create()]);        
    }

    /**
     * Inscription des utilisateurs
     *
     * @return void
     */
    public function register(){
        
        //on vérifie si le formulaire est valide 
        if(!empty($_POST)):

            if(Form::validateRegister($_POST, ['email', 'password'])):
                //le formulaire est valide
                //On nettoie l'email
                $email = strip_tags($_POST['email']);

                //On chiffre le mot de pass
                $pass = password_hash($_POST['password'], PASSWORD_ARGON2I);

                // on hydrate l'utilisateur en base de données

                $user = new UsersModel;
                $user->setEmail($email)->setPassword($pass);

                //on stocke l'utlisateur
                $user->create();
                
                //On confirme l'inscription à l'utilisateur 

                $_SESSION['message'] = 'Votre inscription est confirmé.';
                header('Location: /');
                exit;
            else:
            echo 'Erreur, les champs ne sont pas remplis correctement ou le mot de passe fait moins de 5 caractères';
            endif;
        
        else:
            echo '';
        endif;

        
        $form = new Form;

        $form->debutForm()
            ->ajoutLabelFor('email', 'E-mail :')
            ->ajoutInput('email', 'email', ['id' => 'email', 'class' => 'form-control', 'placeholder' => 'E-mail'])
            ->ajoutLabelFor('pass', 'Mot de passe :')
            ->ajoutInput('password', 'password', ['id' => 'pass', 'class' => 'form-control'])
            ->ajoutLabelFor('pass2', 'Confirmez le mot de passe :')
            ->ajoutInput('password', 'pass2', ['id' => 'pass2', 'class' => 'form-control'])
            ->ajoutBouton('Inscription', ['class' => 'btn btn-primary'])
            ->finForm();

            $this->render('users/register', ['registerForm' => $form->create()]);
    }

    /**
     * déconnexion de l'utilisateur
     *
     * @return exit
     */
    public function logout () 
    {
        unset($_SESSION['user']);
        header('Location: '. $_SERVER['HTTP_REFERER']);
        exit;
    }
}