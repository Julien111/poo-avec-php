<?php

namespace App\Controllers;

use App\Core\Form;
use App\Models\AnnonceModel;

class AnnoncesController extends Controller 
{
    /**
     * Cette méthode affichera toutes les annonces de la base de données
     *
     * @return void
     */
    public function index()
    {
        //On va instancier le modèle correspondant à la table annonces
        $annonceModel = new AnnonceModel;

        //on va chercher toutes les annononces active avec findBy
        $annonces = $annonceModel->findBy(['actif' => 1]);

        //on génère la vue
        $this->render('annonces/index', compact('annonces'));
    }
    
    /**
     * Affiche 1 annonce  
     *
     * @param int $id id de l'annonce
     * @return void
     */
    public function lire (int $id)
    {
        //On instancie le modèle
        $annonceModel = new AnnonceModel;
        
        // on va chercher une annonce
        $annonce = $annonceModel->find($id);

        $this->render('annonces/lire', compact('annonce'));
    }

    /**
     * Ajouter une annonce
     *
     * @return void
     */
    public function ajouter ()
    {
        //On vérifie si l'utilisateur est connecté

        if(isset($_SESSION['user']) && !empty($_SESSION['user']['id'])){

            //L'utilisateur est connecté
            
            //on vérifie si le formulaire est complet qd un user en crée un
            
            
            if(Form::validate($_POST, ['titre', 'description']) && $_SERVER['REQUEST_METHOD'] == 'POST'){
                //Le formulaire est complet
                //On doit traiter les champs sanitize 

                $titre = strip_tags($_POST['titre']);
                $description = strip_tags($_POST['description']);

                //On instancie notre modèle 
                $annonce = new AnnonceModel;
                
                //on hydrate
                $annonce->setTitre($titre)->setDescription($description)->setUsers_id($_SESSION['user']['id']);

                //On enregistre 
                $annonce->create();

                $_SESSION['message'] = 'Votre annonce a été enregistrée avec succès.';
                header('Location: /');
                exit;
            }
            else if(isset($_POST['titre']) && isset($_POST['description']) && $_SERVER['REQUEST_METHOD'] == 'POST'){
                // $titre = isset($_POST['titre']) ? strip_tags($_POST['titre']) : ''; ce code permettrait : si un seul champ est rempli, de sauvegarder ce champ et de dire vous devez remplir tous les champs
                // $description = isset($_POST['description']) ? strip_tags($_POST['description']) : '';
                $_SESSION['erreur'] = 'Le formulaire n\'a pas été rempli correctement.';
                header('Location: /annonces/ajouter');
                exit;
            }
            else{
            $form = new Form;

            $form->debutForm()
                ->ajoutLabelFor('titre', 'Titre de l\'annonce : ')
                ->ajoutInput('text', 'titre', ['id' => 'titre', 'class' => 'form-control'])
                ->ajoutLabelFor('description', 'Contenu de l\'annonce : ')
                ->ajoutTextarea('description', '', ['class' => 'form-control'])
                ->ajoutBouton('Ajouter', ['class' => 'btn btn-primary'])
                ->finForm();

                $this->render('annonces/ajouter', ['form' => $form->create()]);
            }
        }
        else{
            //l'utilisateur n'est pas connecté 
            $_SESSION['erreur'] = "Vous devez être connecté(e) pour accéder à cette page.";
            header("Location: /users/login");
            exit;
        }
    }

    /**
     * Modifier une annonce 
     * @param int $id
     * @return void
     */

    public function modifier (int $id)
    {
        //On vérifie si l'utilisateur est connecté 

        if(isset($_SESSION['user']) && !empty($_SESSION['user']['id'])){

            //L'utilisateur est connecté

            //On va vérifier si l'annonce existe en BDD

            $annonceModel = new AnnonceModel;

            //on charche l'annonce via son id 
            $annonce = $annonceModel->find($id);

            //Si l'annonce n'existe pas on retourne à la liste des annonces
            if(!$annonce){
                http_response_code(404);
                $_SESSION['erreur'] = "L'annonce recherchée n'existe pas.";
                header("Location: /annonces");
                exit;
            }
            //on vérifie si l'utilisateur est propriétaire de l'annonce ou admin
            if($annonce->users_id != $_SESSION['user']['id']){
                if(!in_array('ROLE_ADMIN', $_SESSION['user']['roles'])){
                    $_SESSION['erreur'] = "Vous n'avez pas accès à cette page (annonce).";
                    header("Location: /users/login");
                    exit;
                }
            }

            //On traite le formulaire 

            if(Form::validate($_POST, ['titre', 'description'])){
                $titre = strip_tags($_POST['titre']);
                $description = strip_tags($_POST['description']);
                
                //On stocke l'annonce modifié 

                $annonceModif = new AnnonceModel;

                // on hydrate 

                $annonceModif->setId($annonce->id)
                    ->setTitre($titre)
                    ->setDescription($description);

                //On met à jour l'annonce et on redirige

                $annonceModif->update();

                $_SESSION['message'] = 'Votre annonce a été modifiée avec succès.';
                header('Location: /');
                exit;                
            }

            $form = new Form;

            $form->debutForm()
                ->ajoutLabelFor('titre', 'Titre de l\'annonce : ')
                ->ajoutInput('text', 'titre', ['id' => 'titre', 'class' => 'form-control', 'value' => $annonce->titre])
                ->ajoutLabelFor('description', 'Contenu de l\'annonce : ')
                ->ajoutTextarea('description', $annonce->description, ['class' => 'form-control'])
                ->ajoutBouton('Modifier', ['class' => 'btn btn-primary'])
                ->finForm();

                // on envoit à la vue

                $this->render('annonces/modifier', ['form' => $form->create()]);
            

        }
        else{
            //l'utilisateur n'est pas connecté 
            $_SESSION['erreur'] = "Vous devez être connecté(e) pour accéder à cette page.";
            header("Location: /users/login");
            exit;
        }
    }
}