<?php  

namespace App\Controllers;

use App\Models\AnnonceModel;

class AdminController extends Controller 
{
    public function index ()
    {
        //on doit vérifier qu'on est le role admin
        if($this->isAdmin()){
            $this->render('admin/index', [], 'admin');
        }
    }

    /**
     * Va afficher la listes des annonces sous forme de tableau
     *
     * @return void
     */
    public function annonces ()
    {
        if($this->isAdmin()){
            $annonceModel = new AnnonceModel;
            $annonces = $annonceModel->findAll();

            $this->render('admin/annonces', compact('annonces'), 'admin');

        }
    }

    /**
     * Supprime une annonce si on est admin
     *
     * @param integer $id
     * @return void
     */
    public function supprimeAnnonce(int $id) 
    {
        if($this->isAdmin()){
            $annonce = new AnnonceModel;

            $annonce->delete($id);

            header('Location: '.$_SERVER['HTTP_REFERER']);
        }
    }

    /**
     * Active ou désactive une annonce
     *
     * @param integer $id
     * @return void
     */
    public function activeAnnonce(int $id)
    {
        //On vérifie si on est admin

        if($this->isAdmin()){
            $annoncesModel = new AnnonceModel;

            // on sélectionne le model avec find 

            $annonceArray = $annoncesModel->find($id);

            if($annonceArray){
                $annonce = $annoncesModel->hydrate($annonceArray);

                //si dans la BDD la valeur est 1 (donc true) on la passe à zéro et vice versa

                if($annonce->getActif()){
                    $annonce->setActif(0);
                }
                else{
                    $annonce->setActif(1);
                }

                $annonce->update();
            }
        }
    }

    /**
     * Vérifie si on est admin
     *
     * @return boolean
     */
    public function isAdmin ()
    {
        //On vérifie si on est connecté et que le user a le rôle admin
        if(isset($_SESSION['user']) && in_array('ROLE_ADMIN', $_SESSION['user']['roles'])){
            //On est admin
            return true;
        }
        else{
            //on pas admin
            $_SESSION['erreur'] = 'Vous n\'avez pas accès à cette zone du site.';
            header("Location: /");
            exit;
        }
    }
}