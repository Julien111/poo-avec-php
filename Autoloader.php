<?php

namespace App;

class Autoloader
{
    static function register()
    {
        spl_autoload_register([
            __CLASS__,
            'autoload',
        ]);
    }

    static function autoload($class)
    {
        //On récupère la totalité du namespace de la classe concernée, on va enlever une partie du chemin (App\ pour simplifier)
        //on retire App\ et on a (Client\Compte)
        $class = str_replace(__NAMESPACE__ . '\\', '', $class);
        
        //on remplace les \ par des /
        $class = str_replace('\\', '/', $class);

        // $fichier stock le chemin d'accès total de la class
        $fichier = __DIR__ . '/' . $class . '.php';
        
        //On vérifie que la classe existe (le fichier)
        if(file_exists($fichier)){
            require_once $fichier;
        }

    }
}